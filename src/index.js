import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { ThemeProvider, CSSReset } from '@chakra-ui/core';

// firebase
import firebase from 'firebase/app';
import firebaseConfig from './config/firebaseConfig';
firebase.initializeApp(firebaseConfig);

// chakra-ui

const Chakra = ({ children }) => (
  <ThemeProvider>
    <CSSReset />
    { children }
  </ThemeProvider>
);

ReactDOM.render(
  <Chakra>
    <App />
  </Chakra>, document.getElementById('root'));

serviceWorker.unregister();
