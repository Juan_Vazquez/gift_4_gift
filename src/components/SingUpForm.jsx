import React, { Component } from 'react';
import { FormControl, FormLabel, Input, Flex} from "@chakra-ui/core";

class SingUpForm extends Component{
    render()    {
        return(
            <Flex bg = "blue.50" style = {{width:"35%", }} justify = "center">
                <FormControl isRequired>
                    <FormLabel 
                        htmlFor="fname" 
                        style={{ marginTop:"5%" }}>
                            Nombre
                    </FormLabel>
                    <Input 
                        placeholder = "Nombre" 
                        style={{ width: 400 }}/>
                    <FormLabel 
                        htmlFor="fname" 
                        style={{ marginTop:"5%" }}>
                            Apellido paterno
                    </FormLabel>
                    <Input 
                        placeholder = "Apellido paterno" 
                        style={{ width: 400 }}/>
                    <FormLabel 
                        htmlFor="fname" 
                        style={{ marginTop:"5%" }}>
                            Apellido materno
                    </FormLabel>
                    <Input 
                        placeholder = "Apellido materno" 
                        style={{ width: 400 }}/>
                    <FormLabel 
                        htmlFor="fname" 
                        style={{ marginTop:"5%" }}>
                            Usuario
                    </FormLabel>
                    <Input 
                        placeholder = "Nombre de usuario" 
                        style={{ width: 400 }}/>
                    <FormLabel 
                        htmlFor="fname" 
                        style={{ marginTop:"5%" }}> 
                        Correo electrónico
                    </FormLabel>
                    <Input 
                        placeholder = "Correo electrónico" 
                        style={{ width: 400 }}/>
                    <FormLabel 
                        htmlFor="fname" 
                        style={{ marginTop:"5%" }}>
                             Confirmar contraseña
                    </FormLabel>
                    <Input 
                        placeholder = "Confirmar contraseña" 
                        style={{ width: 400, marginBottom:"10%" }}/>
                </FormControl>
            </Flex>
        )
    }
}

export default SingUpForm