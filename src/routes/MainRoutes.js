import React from 'react'
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Switch } from 'react-router-dom';

// Views
import HomeView from '../view/HomeView/HomeView'
import SingUpView from '../components/SingUpForm'

const MainRoutes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={SingUpView} /> 
    </Switch>
  </Router>
)

export default MainRoutes;
